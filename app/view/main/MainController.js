/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('BloodDonor.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

      onItemSelected: function (sender, record) {
        Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
    },

    onClickButton : function(){
        Ext.Msg.confirm('Logout', ' Apakah Anda Ingin Logout?', 'onConfirm', this);
    },

    onDatadipilih: function (sender, record) {
        var nama = record.data.name;
        var npm = record.data.npm;
        localStorage.setItem('nama', nama);
        localStorage.setItem('npm', npm);
        Ext.Msg.confirm('Pastikan Lagi  ', 'Sudah Yakin '+nama+'?', 'onConfirm', this);
        console.log(record.data);
    },

    onConfirm: function (choice) {
        var nama = localStorage.getItem('nama');
        var npm = localStorage.getItem('npm');
        if (choice === 'yes') {
            alert ('pilihan anda sudah masuk, '+nama+' ('+npm+')');
        }
        else {
            alert ('batal memilih, '+nama+' ('+npm+') ya');
        }
    },
     onInputChange : function(newValue, oldValue){
        keyword = Ext.getCmp('search').getValue();
        personnelStore = Ext.getStore('personnel');
        personnelStore.filter('name', keyword);
    },
});     