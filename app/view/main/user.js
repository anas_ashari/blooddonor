/**
 * Demonstrates a tabbed form panel. This uses a tab panel with 3 tabs - Basic, Sliders and Toolbars - each of which is
 * defined below.
 *
 * See this in action at http://dev.sencha.com/deploy/sencha-touch-2-b3/examples/kitchensink/index.html#demo/forms
 */
Ext.define('BloodDonor.view.main.user', {
    extend: 'Ext.form.Panel',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Spinner',
        'Ext.field.Password',
        'Ext.field.Email',
        'Ext.field.Url',
        'Ext.field.DatePicker',
        'Ext.field.Select',
        'Ext.field.Hidden',
        'Ext.field.Radio'
    ],
    shadow: true,
    cls: 'demo-solid-background',
    xtype: 'user',
    id: 'user',
    items: [
        {
            xtype: 'fieldset',
            id: 'fieldset1',
            title: 'Formulir Pendaftaran',
            instructions: 'Pastikan Data Yang Anda Masukkan Benar, Jika Sudah Sesuai Silahkan klik tombol Daftar',
            defaults: {
                labelWidth: '90%'
            },
            items: [
            {
                     xtype: 'textfield',
                    name: 'id',
                    label: 'ID',
                    placeHolder: 'ex 001',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'textfield',
                    name: 'nama',
                    label: 'Nama',
                    placeHolder: 'Tom Roy',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'emailfield',
                    name: 'email',
                    label: 'Email',
                    placeHolder: 'me@sencha.com',
                    clearIcon: true
                },
                {
                    xtype: 'textfield',
                    name: 'alamat',
                    label: 'Alamat',
                    placeHolder: 'Jl. Soekano Hatta',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'textfield',
                    name: 'No Telp',
                    label: 'No Telp',
                    placeHolder: '+628 ** **** ****',
                    clearIcon: true
                },
                {
                    xtype: 'spinnerfield',
                    name: 'usia',
                    label: 'Usia',
                    minValue: 0,
                    maxValue: 100,
                    clearable: true,
                    stepValue: 1,
                    cycle: true
                },
                {
                    xtype: 'datepickerfield',
                    destroyPickerOnHide: true,
                    name: 'tglpendaftaran',
                    label: 'Tanggal Pendaftaran',
                    value: new Date(),
                    picker: {
                        yearFrom: 1990
                    }
                },
                {
                    xtype: 'selectfield',
                    name: 'goldar',
                    label: 'Golongan Darah',
                    options: [
                        {
                            text: 'A',
                            value: 'a'
                        },
                        {
                            text: 'B',
                            value: 'b'
                        },
                        {
                            text: 'AB',
                            value: 'ab'
                        },
                        {
                            text: 'O',
                            value: 'o'
                        }
                    ]
                },
                {
                    xtype: 'textareafield',
                    name: 'keterangan',
                    label: 'Keterangan'
                }
            ]
        },
        {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    text: 'Daftar',
                    ui: 'action',
                    scope: this,
                    hasDisabled: false,
                    handler: function(btn){
                        Ext.Msg.alert('Data Tersimpan',' Data Tersimpan ', Ext.emptyFn);                    }
                },
                {
                    text: 'Reset',
                    ui: 'action',
                    handler: function(){
                        Ext.getCmp('basicform').reset();
                    }
                },
                {
                    text: 'Back To Home',
                    ui: 'action',
                    handler: function(){
                        alert('kita akan pindah ke home');
                        var mainView = Ext.getCmp('app-main');
                        mainView.setActiveItem(0);
                    }
                }
                
            ]
        }
    ]
});