/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('BloodDonor.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    id: 'app-main',

    requires: [
        'Ext.MessageBox',

        'BloodDonor.view.main.MainController',
        'BloodDonor.view.main.MainModel',
        'BloodDonor.view.main.List',
        'BloodDonor.view.main.user',
        'BloodDonor.view.main.pasien',
        'BloodDonor.view.form.login',
        'BloodDonor.view.form.loginController',
        'BloodDonor.view.more.basicdataview'
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
           {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items:[
            {
            xtype: 'toolbar',
            docked: 'bottom',
            scrollable: {
                y: false
            },
            items:[
            {
            xtype: 'button',
            text : 'Informasi',
            ui: 'action',
            handler: function (){
                Ext.Msg.alert('Informasi','Blood Donor adalah wadah yang memberikan bantuan berupa donor darah, kamu juga bisa mendonorkan darah dengan mengisi data dan akan disumbangkan ke rumah sakit terutama bagi saudara kita yang membutuhkannya.');
            }
            }]
        },
            {
                xtype: 'mainlist'
            }]
        },{
            title: 'Register',
            iconCls: 'x-fa fa-user',
           layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'user'
            }]
        },{
       title: 'Pasien',
            iconCls: 'x-fa fa-bookmark',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items:[
            {
            xtype: 'toolbar',
            docked: 'bottom',
            scrollable: {
                y: false
            },
            items:[
            {
            xtype: 'button',
            text : 'Informasi',
            ui: 'action',
            handler: function (){
                Ext.Msg.alert('Informasi','Blood Donor adalah wadah yang memberikan bantuan berupa donor darah, kamu juga bisa mendonorkan darah dengan mengisi data dan akan disumbangkan ke rumah sakit terutama bagi saudara kita yang membutuhkannya.');
            }
            }]
        },
            {
                xtype: 'pasien'
            }]
        },{
            title: 'More',
            iconCls: 'x-fa fa-ellipsis-h',
             layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'bdv'
            }]
        }
    ]
});
    