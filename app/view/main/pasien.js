/**
 * Demonstrates a very simple tab panel with 3 tabs
 */
Ext.define('BloodDonor.view.main.pasien', {
    extend: 'Ext.tab.Panel',
    xtype: 'pasien',
    shadow: true,
    cls: 'demo-solid-background',
    tabBar: {
        layout: {
            pack: 'center'
        }
    },
    activeTab: 1,
    defaults: {
        scrollable: true
    },
    items: [
        {
            title: 'Golongan Darah A',
            html : 'asoasjajospaosjoa.',
            cls: 'card'
        },
        {
            title: 'Golongan Darah B',
            html : 'A TabPanel can use different animations by setting <code>layout.animation.</code>',
            cls: 'card'
        },
        {
            title: 'Golongan Darah AB',
            html : '<span class="action">User tapped Tab 3</span>',
            cls: 'card'
        },
        {
            title: 'Golongan Darah O',
            html : '<span class="action">User tapped Tab 3</span>',
            cls: 'card'
        }
    ]
});