/**
 * This view is an example list of people.
 */
Ext.define('BloodDonor.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'BloodDonor.store.Personnel'
    ],

    title: 'aplikasi donor darah',

    store: {
        type: 'personnel'
    },

    columns: [
        { text: 'Name',  dataIndex: 'name', width: 200 },
        { text: 'Email', dataIndex: 'email', width: 290 },
        { text: 'Phone', dataIndex: 'phone', width: 250 },
        { text: 'Alamat',  dataIndex: 'alamat', width: 1000 }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
