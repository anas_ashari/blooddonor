Ext.define('BloodDonor.view.more.basicdataview', {
    extend: 'Ext.Container',
    xtype: 'bdv',
    requires: [

        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive'
    ],

    
    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    viewModel:{
        stores:{
            personnel:{
                type: 'personnel'
            }
        }
    },
    items: [{
        xtype: 'panel',
        scrollable: 'y',

            items: [
        {
            xtype: 'toolbar',
            docked: 'top',
            scrollable: {
                y: false
            },
            items: [

                {
                    xtype: 'spacer'
                },
                {
                    xtype: 'segmentedbutton',
                    allowDepress: true,
                    items: [

                        {
                            xtype:'button',
                            ui:'action',
                            text: 'Help',
                            handler:function(btn){
                              Ext.Msg.alert('HELP','Informasi Lebih Lanjut Hubungi 082351500262',Ext.emptyFn)
                            }
                        }
                    ]
                },
                {
                    xtype: 'spacer'
                },
                {
                  xtype: 'button',
                  reference: 'button',
                  text: 'Button',
                  text: 'Logout',
                  margin: '0 0 16 0',
                  handler: 'onClickButton'
                },
                {
                         docked :'top',
                             xtype: 'toolbar',
                             items: [
                             {
                                 xtype: 'searchfield',

                                 placeHolder: 'Search Nama',
                                 name: 'searchfield',
                                 id: 'search',
                                 listeners: {
                                     change: 'onInputChange'
                                }
                            }
                        ]
                }
            ]
        }
    ],
        xtype: 'dataview',
        scrollable: 'y',
        cls: 'dataview-basic',
        itemTpl: '<div class="card" style="width:300px">' +
        '<img class="card-img-top">{photo}' +
        '<div class="card-body">' +
        '<h1 class="card-title">Name : <b>{name}</b></h2>' +
        '<p class="card-text"> Email : {email}</p>' +
        '<p class="card-text"> Phone : {phone}</p>' +
        '<p class="card-text"> Alamat : <font size=2 color="red">{alamat}</p></font>' +
        '<button class="btn btn-primary">Detail</button>' +
        '</div>' +
        '</div><br>',
         bind:{
            store: '{personnel}'
        },
        plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            delegate: '.img',
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
                    '<tr><td>Affiliation: </td><td>{affiliation}</td></tr>' +
                    '<tr><td>Position:</td><td>{position}</td></tr>' + 
                    '<tr><td vAlign="top">Bio:</td><td><div style="max-height:100px;overflow:auto;padding:1px">{bio}</div></td></tr>'
        }
    }],

    listeners: {
        click: {
        element :'element',
        delegate : '.btn-primary',
        fn: function (out, values, parent, xindex, xcount, xkey){
            var data = values.offsetParent.innerHTML;
            Ext.Msg.alert('Detail','Ini adalah Detail', Ext.emptyFn);
        }
    }
}
});