Ext.define('BloodDonor.view.form.loginController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.login',

    onLogin : function(){
        var form = this.getView();
        var user = "admin";
        var pass = "admin";
        var username = form.getFields('username').getValue();
        var password = form.getFields('password').getValue();
        var mainView = Ext.getCmp('user');

        if(username == user && password == pass){
            alert ('Welcome BloodDonor System')
                localStorage.setItem('logeddin',true);
                form.hide();
        }
        else{
            alert('Username/Password Salah')
        }
    },
    onRegis: function() {
                     if (!this.overlay) {
                         this.overlay = Ext.Viewport.add({
                             xtype: 'panel',
                             floated: true,
                             modal: true,
                            hideOnMaskTap: true,
                            showAnimation: {
                                type: 'popIn',
                               duration: 250,
                               easing: 'ease-out'
                            },
                           hideAnimation: {
                               type: 'popOut',
                               duration: 250,
                                easing: 'ease-out'
                             },
                             centered: true,
                             width :"80%",
                            height :"80%",

                                 title: 'Pendaftaran Member Baru',
                                 id:'regis',
                                items:[
                                    {
                                        label:'Nama',
                                        xtype:'textfield',
                                        name:'username',
                                    },
                                    {
                                        label:'password',
                                        xtype:'passwordfield',
                                        name:'password',
                                    },
                                    {
                                        label:'Domisili',
                                        xtype:'textfield',
                                        name:'Domisili',
                                    },
                                      {
                                       xtype: 'datepickerfield',
                                        destroyPickerOnHide: true,
                                        name: 'Tahun Lahir',
                                        label: 'Tahun Lahir',
                                        value: new Date(),
                                        picker: {
                                        yearFrom: 1990
                                    }
                                    },
                                    {
                                        label:'Usia',
                                        xtype:'textfield',
                                        name:'usia',
                                    },
                                    {
                                        label:'Tanggal Lahir',
                                        xtype:'textfield',
                                        name:'ttl',
                                    },
                                     {
                                     xtype: 'selectfield',
                                     name: 'Status',
                                     label: 'status',
                                    options: [
                            {
                                text: 'Belum Menikah',
                                value: 'Belum Menikah'
                            },
                            {
                                text: 'Menikah',
                                value: 'Menikah'
                            }
                             ]
                            },
                                    {
                                        xtype:'button',
                                        text: 'Daftar',
                                        ui: 'action',
                                        handler: function(){
                                            Ext.Msg.alert('Register Success','You have been Registered');
                                        }
                                    },
                                    {
                                         xtype:'button',
                                         text: 'Kembali',
                                         style: 'margin: 1em 0 1em 1em',
                                         style:'background-color:#E61607;margin:14px;color: pink',
                                      ui: 'action',
                                         handler: function(){
                                             var reg = Ext.getCmp('regis');
                                             reg.close();
                                           }
                                    }
                                ],


                             scrollable: true
                         });
                     }this.overlay.show();
         },

});