Ext.define('BloodDonor.view.form.login', {
    extend: 'Ext.form.Panel',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Password'
    ],
    shadow: true,
    cls: 'demo-solid-background',
    xtype: 'login',
    controller : 'login',
    id: 'login',
    items: [
        {
            xtype: 'fieldset',
            id: 'fieldsetlogin',
            title: 'LOGIN FORM',
            instructions: 'Please enter the information above.',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'username',
                    label: 'UserName',
                    placeHolder: 'Tom Roy',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'passwordfield',
                    revealable: true,
                    name : 'password',
                    label: 'Password',
                    clearIcon: true
                }
            ]
        },
        {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    text: 'Login',
                    ui: 'action',
                    //scope: this,
                    hasDisabled: false,
                    handler: 'onLogin'
                },{
                text: 'Reset',
                    ui: 'action',
                    //scope: this,
                    hasDisabled: false,
                    handler: 'onLogin'
                }, {
                         text: 'Register Member',
                         ui: 'action',
                         handler: 'onRegis',
                    },

            ]
        }
    ]
});