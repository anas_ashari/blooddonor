Ext.define('BloodDonor.store.Personnel', {
    extend: 'Ext.data.Store',
    storeId: 'personnel',

    alias: 'store.personnel',

    fields: [
        'name', 'email', 'phone', 'alamat', 'photo'
    ],

    data: { items: [
        { photo: '<img src="resources/images/awalbros.jpg" width="300px" height"300px">', name: 'RS Awal Bros',      email: "awalbroscare.com",          phone: "(0761) 47333",  alamat: 'Jl. Sudirman, Tangkerang Kec.Bukit Raya. Pekanbaru'},
        { photo: '<img src="resources/images/aulia.jpg" width="300px" height"300px">',name: 'Aulia Hospital',    email: "auliahospital.com",         phone: "(0761) 670001", alamat: 'Jl. Hr.Soebrantas Panam, Sidomulyo Barat, Kec.Tampan. Pekanbaru' },
        { photo: '<img src="resources/images/arifinahmad.jpg" width="300px" height"300px">',name: 'RSUD Arifin Achmad',email: "RSUDarifinAchmad.com",      phone: "(0761) 21618",  alamat: 'Jl. Diponegoro, Sumahilang, Kec.Pekanbaru Kota. Pekanbaru' },
        { photo: '<img src="resources/images/ekahospital.jpg" width="300px" height"300px">',name: 'Eka Hospital',      email: "ekahospitalcare.com",       phone: "(0761) 6989999",alamat: 'Jl. Soekarno Hatta, Tangkerang Barat. Kec. Marpoyan Damai. Pekanbaru' },
        { photo: '<img src="resources/images/sansani.jpg" width="300px" height"300px">',name: 'RS Sansani',        email: "sansanipb.com",             phone: "(0761) 564666", alamat: 'Jl. Soekarno Hatta, Air Hitam, Kec.Payung Sekaki. Pekanbaru' },
        { photo: '<img src="resources/images/santamaria.jpg" width="200px" height"150px">',name: 'RS Santa Maria',    email: "santamaria22.com",          phone: "(0761) 22213",  alamat: 'Jl. Ahmad Yani, Pulau Karam. Kec. Sukajadi. Pekanbaru' },
        { photo: '<img src="resources/images/syafira.jpg" width="300px" height"300px">', name: 'RS Syafira',        email: "safira22.com",              phone: " 0822-1000-3636",alamat: 'Jl. Jend. Sudirman No.134, Tengkerang Tengah, Kec. Marpoyan Damai, Kota Pekanbaru.' },
        { photo: '<img src="resources/images/zainab.jpg" width="300px" height"300px">',name: 'RS Zainab',         email: "zainabkotapku.com",         phone: "(0761)24000",   alamat: 'Jl. Ronggo Warsito No.1, Suka Maju, Kec. Sail, Kota Pekanbaru' },
        { photo: '<img src="resources/images/ibnusina.jpg" width="300px" height"300px">',name: 'RS Ibnu Sina',      email: "ibunusinabpjs.com",         phone: "(0761) 24242",  alamat: 'Jl. Melati No.60, Harjosari, Kec. Sukajadi, Kota Pekanbaru' },
        { photo: '<img src="resources/images/plitabumi.jpg" width="300px" height"300px">',name: 'RSUD Pelata Bumi',  email: "pelatabumi0.com",           phone: " (0761) 23024", alamat: 'Jl. Tuanku Tambusai No.Rt03/02, Delima, Kec. Tampan, Kota Pekanbaru' },
        { photo: '<img src="resources/images/hermina.jpg" width="300px" height"300px">',name: 'RS Hermina Pekanbaru',email: "herminapekanbaru.com",    phone: "(0761) 8412020",alamat: 'Jl. Ahmad Yani, Pulau Karam. Kec. Sukajadi. Pekanbaru' },
        { photo: '<img src="resources/images/Muhammadiyah.jpg" width="300px" height"300px">',name: 'RS Muhammadiyah',     email: "muhammadiyahnangka.com",  phone: "(0761) 22213",  alamat: 'Tambusai, Jl. Nangka, Delima, Kec. Tampan, Kota Pekanbaru' }
    ]},
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
